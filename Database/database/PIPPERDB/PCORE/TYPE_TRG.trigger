<?xml version = '1.0' encoding = 'UTF-8'?>
<trigger xmlns="http://xmlns.oracle.com/jdeveloper/1111/offlinedb">
  <ID class="oracle.javatools.db.IdentifierBasedID">
    <identifier class="java.lang.String">cc9724f3-6fcd-4b1b-94b4-811fd9308bc4</identifier>
  </ID>
  <name>TYPE_TRG</name>
  <baseType>TABLE</baseType>
  <code>BEGIN
  &lt;&lt;COLUMN_SEQUENCES&gt;&gt;
  BEGIN
    IF :NEW.TYPE_ID IS NULL THEN
      SELECT TYPE_SEQ.NEXTVAL INTO :NEW.TYPE_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;</code>
  <enabled>true</enabled>
  <events>
    <event>INSERT</event>
  </events>
  <properties>
    <entry>
      <key>TemplateObject</key>
    </entry>
  </properties>
  <schema>
    <name>PCORE</name>
  </schema>
  <source>CREATE TRIGGER TYPE_TRG 
BEFORE INSERT ON TYPE 
FOR EACH ROW 
BEGIN
  &lt;&lt;COLUMN_SEQUENCES&gt;&gt;
  BEGIN
    IF :NEW.TYPE_ID IS NULL THEN
      SELECT TYPE_SEQ.NEXTVAL INTO :NEW.TYPE_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;</source>
  <statementLevel>false</statementLevel>
  <tableID class="oracle.javatools.db.IdentifierBasedID">
    <name>TYPE</name>
    <identifier class="java.lang.String">aa0a0363-3b26-4e9d-9e62-baeb1cf3690b</identifier>
    <schemaName>PCORE</schemaName>
    <type>TABLE</type>
  </tableID>
  <timing>BEFORE</timing>
</trigger>
