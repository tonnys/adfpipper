<?xml version = '1.0' encoding = 'UTF-8'?>
<trigger xmlns="http://xmlns.oracle.com/jdeveloper/1111/offlinedb">
  <ID class="oracle.javatools.db.IdentifierBasedID">
    <identifier class="java.lang.String">ed2f3dd0-929d-4565-800f-50afc463d2d6</identifier>
  </ID>
  <name>ITEM_TRG</name>
  <baseType>TABLE</baseType>
  <code>BEGIN
  &lt;&lt;COLUMN_SEQUENCES&gt;&gt;
  BEGIN
    IF :NEW.ITEM_ID IS NULL THEN
      SELECT ITEM_SEQ.NEXTVAL INTO :NEW.ITEM_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;</code>
  <enabled>true</enabled>
  <events>
    <event>INSERT</event>
  </events>
  <schema>
    <name>PCORE</name>
  </schema>
  <source>CREATE TRIGGER ITEM_TRG 
BEFORE INSERT ON ITEM 
FOR EACH ROW 
BEGIN
  &lt;&lt;COLUMN_SEQUENCES&gt;&gt;
  BEGIN
    IF :NEW.ITEM_ID IS NULL THEN
      SELECT ITEM_SEQ.NEXTVAL INTO :NEW.ITEM_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;</source>
  <statementLevel>false</statementLevel>
  <tableID class="oracle.javatools.db.IdentifierBasedID">
    <name>ITEM</name>
    <identifier class="java.lang.String">1aa18506-24da-44bf-9aaf-2940dcd26538</identifier>
    <schemaName>PCORE</schemaName>
    <type>TABLE</type>
  </tableID>
  <timing>BEFORE</timing>
</trigger>
